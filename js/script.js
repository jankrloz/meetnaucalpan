jQuery(document).ready(function($) {

	if($(window).innerWidth() > 1024){
		scrolleffect();

		$('.icon').mouseover(function(event) {
			$(this).addClass('animated pulse');
		});

		$('.icon').mouseout(function(event) {
			$(this).removeClass('animated pulse');
		});
	}
	else{
		$('.scrolleffect').css('opacity', '1');
	}
});